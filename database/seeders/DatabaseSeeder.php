<?php

namespace Database\Seeders;

use App\Models\Customer;
use App\Models\Menu;
use App\Models\Role;
use App\Models\Produk;
use App\Models\Submenu;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        Role::create([
            'name' => 'PJ'
        ]);
        Role::create([
            'name' => 'Root'
        ]);
        Role::create([
            'name' => 'Pembeli'
        ]);
        Role::create([
            'name' => 'Penjual'
        ]);
        Role::create([
            'name' => 'Admin'
        ]);

        Menu::create([
            'name' => 'Root'
        ]);
       
        Submenu::create([
            'menu_id' => 1,
            'title' => 'Dashboard',
            'url' => '/dashboard',
            'icon' => 'far fa-clock',
            'is_active' => 1,
        ]);
        
        Submenu::create([
            'menu_id' => 1,
            'title' => 'Master Menu',
            'url' => '/menu',
            'icon' => 'fas fa-clipboard-list',
            'is_active' => 1,
        ]);
        Submenu::create([
            'menu_id' => 1,
            'title' => 'Submenu Management',
            'url' => '/submenu',
            'icon' => 'fas fa-list',
            'is_active' => 1,
        ]);
        Submenu::create([
            'menu_id' => 1,
            'title' => 'Master Produk',
            'url' => '/produk',
            'icon' => 'fas fa-box',
            'is_active' => 1,
        ]);
        Submenu::create([
            'menu_id' => 1,
            'title' => 'Master Customer',
            'url' => '/customer',
            'icon' => 'fas fa-users',
            'is_active' => 1,
        ]);
        Submenu::create([
            'menu_id' => 1,
            'title' => 'Sales',
            'url' => '/sales',
            'icon' => 'fas fa-chart-line',
            'is_active' => 1,
        ]);
        
        Produk::create([
            'nama_produk' => "Sabun",
            'harga' => 50000
        ]);
        Produk::create([
            'nama_produk' => "Shampo",
            'harga' => 70000
        ]);
        Produk::create([
            'nama_produk' => "Sabun Muka",
            'harga' => 80000
        ]);

        Customer::create([
            'nama_customer' => 'Prayoga Erlangga Putra',
            'alamat' => 'Subang',
            'no_hp' => '0817211495' 
        ]);
        Customer::create([
            'nama_customer' => 'Muksin Alatas',
            'alamat' => 'Subang',
            'no_hp' => '08226543612'
        ]);
    }
}
