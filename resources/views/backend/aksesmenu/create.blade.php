@extends('backend.layouts.main')

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb bg-white">
        <div class="row align-items-center">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{{ $title }}</h4>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-lg-6 col-sm-6">
                <div class="white-box">
                    <div class="d-md-flex mb-3">
                        <h3 class="box-title mb-0">Berikan Akses</h3>
                    </div>
                    <div class="col-lg-8">
                        <form action="/aksesmenu" method="post">
                            @csrf
                            <label for="menu_id" class="mt-2">Menu</label>
                            <select name="menu_id" id="menu_id" class="form-control @error('menu_id') is-invalid @enderror">
                                <option value="" selected disabled>Pilih Menu</option>
                                @foreach ($menu as $menu)
                                <option value="{{ $menu->id }}">{{ $menu->name }}</option>
                                @endforeach
                            </select>
                            @error('menu_id')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                            <label for="role_id" class="mt-2">Untuk Role</label>
                            <select name="role_id" id="role_id" class="form-control @error('role_id') is-invalid @enderror">
                                <option value="" selected disabled>Pilih Role</option>
                                @foreach ($role as $r)
                                <option value="{{ $r->id }}">{{ $r->name }}</option>
                                @endforeach
                            </select>
                            @error('role_id')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                            <button type="submit" class="btn btn-primary mt-3">Tambah</button>
                        </form> 
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Recent Comments -->
        <!-- ============================================================== -->
        
    </div>
@endsection