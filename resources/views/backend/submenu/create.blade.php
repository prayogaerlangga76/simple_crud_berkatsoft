@extends('backend.layouts.main')

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb bg-white">
        <div class="row align-items-center">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{{ $title }}</h4>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-lg-6 col-sm-6">
                <div class="white-box">
                    <div class="d-md-flex mb-3">
                        <h3 class="box-title mb-0">Tambah submenu baru</h3>
                    </div>
                    <div class="col-lg-8">
                        <form action="/submenu" method="post">
                            @csrf
                            <label for="title">Nama Submenu</label>
                            <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" id="title" autofocus>
                            @error('title')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                            <label for="menu_id" class="mt-2">Master Menu</label>
                            <select name="menu_id" id="menu_id" class="form-control @error('menu_id') is-invalid @enderror">
                                <option value="" selected disabled>Pilih menu</option>
                                @foreach ($menu as $menu)
                                <option value="{{ $menu->id }}">{{ $menu->name }}</option>
                                @endforeach
                            </select>
                            @error('menu_id')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                            <label for="icon" class="mt-2">Icon</label>
                            <input type="text" class="form-control @error('icon') is-invalid @enderror" name="icon" id="icon">
                            @error('icon')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                            <label for="url" class="mt-2">Url</label>
                            <input type="text" class="form-control @error('url') is-invalid @enderror" name="url" id="url">
                            @error('url')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                            <label for="is_active" class="mt-3"> Aktifkan Menu ?</label>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="is_active" name="is_active" value="1" class="custom-control-input">
                                <label class="custom-control-label mr-3" for="is_active">Ya</label>
                                
                              </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="is_active" name="is_active" value="2" class="custom-control-input">
                                <label class="custom-control-label" for="is_active">Tidak</label>
                                
                              </div>
                            <button type="submit" class="btn btn-primary mt-3">Tambah</button>
                        </form> 
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Recent Comments -->
        <!-- ============================================================== -->
        
    </div>
@endsection