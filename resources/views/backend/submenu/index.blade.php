@extends('backend.layouts.main')

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb bg-white">
        <div class="row align-items-center">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{{ $title }}</h4>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="white-box">
                    <div class="d-md-flex mb-3">
                        <h3 class="box-title mb-0">Daftar Submenu Aplikasi</h3>
                        <div class="col-md-3 col-sm-4 col-xs-6 ms-auto">
                            <a href="/submenu/create" class="btn btn-primary shadow-none row border-top">Tambah Submenu</a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table no-wrap">
                            <thead>
                                <tr>
                                    <th class="border-top-0">No</th>
                                    <th class="border-top-0">Nama submenu</th>
                                    <th class="border-top-0">Master Menu</th>
                                    <th class="border-top-0">Url</th>
                                    <th class="border-top-0">Icon</th>
                                    <th class="border-top-0">Status</th>
                                    <th class="border-top-0">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($submenu as $m)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $m->title }}</td>
                                        <td>{{ $m->menu->name }}</td>
                                        <td>{{ $m->url }}</td>
                                        <td>{{ $m->icon }}</td>
                                        @if ($m->is_active == 1)
                                        <td><p class="bg-success rounded text-white text-center">Aktif</p></td>
                                        @else
                                        <td><p class="bg-danger rounded text-white text-center">Tidak Aktif</p></td>
                                        @endif
                                        <td class="text-center">
                                            <a href="/submenu/{{ $m->id }}/edit"
                                                class="btn btn-warning shadow-none">Edit</a>
                                            <form action="/submenu/{{ $m->id }}" method="post" class="d-inline">
                                                @method('delete')
                                                @csrf
                                                <button type="submit" class="btn btn-danger text-white shadow-none">Hapus</button>
                                            </form>
                                        </td>
                                    </tr>
                                @empty
                                <tr>
                                    <td colspan="7" class="text-center">Belum ada Data</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
