@extends('backend.layouts.main')

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb bg-white">
        <div class="row align-items-center">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{{ $title }}</h4>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-lg-6 col-sm-6">
                <div class="white-box">
                    <div class="d-md-flex mb-3">
                        <h3 class="box-title mb-0">Tambah Sales</h3>
                    </div>
                    <div class="col-lg-8">
                        <form action="/sales" method="post">
                            @csrf
                            <label for="customer_id" class="mt-2">Customer</label>
                            <select name="customer_id" id="customer_id"
                                class="form-control @error('customer_id') is-invalid @enderror">
                                <option value="" selected disabled>Pilih Customer</option>
                                @foreach ($customers as $c)
                                    <option value="{{ $c->id }}">{{ $c->nama_customer }}</option>
                                @endforeach
                            </select>
                            @error('customer_id')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                            <label for="produk_id1" class="mt-2">Produk 1 <small class="text-muted"><i>(required)</i></small></label>
                            <select name="produk_id1" id="produk_id1"
                                class="form-control @error('produk_id1') is-invalid @enderror">
                                <option value="" selected disabled>Pilih Produk</option>
                                @foreach ($produk as $p)
                                    <option value="{{ $p->id }}">{{ $p->nama_produk }}</option>
                                @endforeach
                            </select>
                            @error('produk_id1')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                            <label for="harga1" class="mt-2">Harga</label>
                            <input type="number" readonly min="1" class="form-control @error('harga1') is-invalid @enderror" name="harga1"
                                id="harga1">
                            @error('harga1')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                            <label for="jumlah1" class="mt-2">Jumlah</label>
                            <input type="number" min="1" class="form-control @error('jumlah1') is-invalid @enderror" name="jumlah1"
                                id="jumlah1">
                            @error('jumlah1')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                            <label for="total1" class="mt-2">Total</label>
                            <input type="number" readonly min="1" class="form-control @error('total1') is-invalid @enderror" name="total1"
                                id="total1">
                            @error('total1')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror

                            <label for="produk_id2" class="mt-2">Produk 2 <small class="text-muted"><i>(optional)</i></small></label>
                            <select name="produk_id2" id="produk_id2"
                                class="form-control @error('produk_id2') is-invalid @enderror">
                                <option value="" selected disabled>Pilih Produk</option>
                                @foreach ($produk as $p)
                                    <option value="{{ $p->id }}">{{ $p->nama_produk }}</option>
                                @endforeach
                            </select>
                            
                            @error('produk_id2')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                            <label for="harga2" class="mt-2">Harga</label>
                            <input type="number" readonly min="1" class="form-control @error('harga2') is-invalid @enderror" name="harga2"
                                id="harga2">
                            @error('harga2')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                            <label for="jumlah2" class="mt-2">Jumlah</label>
                            <input type="number" min="1" class="form-control @error('jumlah2') is-invalid @enderror" name="jumlah2"
                                id="jumlah2">
                            @error('jumlah2')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                            <label for="total2" class="mt-2">Total</label>
                            <input type="number" readonly min="1" class="form-control @error('total2') is-invalid @enderror" name="total2"
                                id="total2">
                            @error('total2')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                            <button type="submit" class="btn btn-primary mt-3">Tambah</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Recent Comments -->
        <!-- ============================================================== -->

    </div>
@endsection
@push('script')
    <script type='text/javascript'>
        $(document).on('change', '#produk_id1', function() {
            $.ajax({
                type: "GET",
                url: '/produk/' + $('#produk_id1').val(),
                dataType: "json",
                success: function(response) {
                    console.log(response);
                    $('#harga1').val(response.harga);
                }

            });
        });
        $(document).on('change', '#jumlah1', function() {
            $.ajax({
                success: function(response) {
                    $('#total1').val($('#jumlah1').val() * $('#harga1').val());
                }

            });
        });
        $(document).on('change', '#produk_id2', function() {
            $.ajax({
                type: "GET",
                url: '/produk/' + $('#produk_id2').val(),
                dataType: "json",
                success: function(response) {
                    console.log(response);
                    $('#harga2').val(response.harga);
                }

            });
        });
        $(document).on('change', '#jumlah2', function() {
            $.ajax({
                success: function(response) {
                    $('#total2').val($('#jumlah2').val() * $('#harga2').val());
                }

            });
        });
    </script>
@endpush