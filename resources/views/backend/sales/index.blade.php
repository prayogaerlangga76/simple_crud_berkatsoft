@extends('backend.layouts.main')

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb bg-white">
        <div class="row align-items-center">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{{ $title }}</h4>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="white-box">
                    <div class="d-md-flex mb-3">
                        <h3 class="box-title mb-0">Daftar sales</h3>
                        <div class="col-md-3 col-sm-4 col-xs-6 ms-auto">
                            <a href="/sales/create" class="btn btn-primary shadow-none row border-top">Tambah Sales</a>
                        </div>
                    </div>
                    @if (session()->has('success'))
                        <div class="alert alert-success col-lg-6" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table no-wrap">
                            <thead>
                                <tr>
                                    <th class="border-top-0">#</th>
                                    <th class="border-top-0">Nama Customer</th>
                                    <th class="border-top-0">Nama produk</th>
                                    <th class="border-top-0">Harga</th>
                                    <th class="border-top-0">Jumlah</th>
                                    <th class="border-top-0">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($sales as $s)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $s->customer->nama_customer }}</td>
                                        <td>{{ $s->produk->nama_produk }}</td>
                                        <td>Rp. {{ number_format($s->produk->harga,0,"",".") }}</td>
                                        <td>{{ $s->jumlah }}</td>
                                        <td>Rp. {{ number_format($s->total,0,"",".") }}</td>
                                    </tr>
                                @empty
                                <tr>
                                    <td colspan="6" class="text-center">Belum ada Data</td>
                                </tr>
                                @endforelse
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="5">Total Sales</th>
                                    <th>Rp. {{ number_format($count,0,"",".") }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Recent Comments -->
        <!-- ============================================================== -->
        
    </div>
@endsection