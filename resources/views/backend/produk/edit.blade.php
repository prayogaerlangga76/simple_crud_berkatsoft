@extends('backend.layouts.main')

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb bg-white">
        <div class="row align-items-center">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{{ $title }}</h4>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-lg-6 col-sm-6">
                <div class="white-box">
                    <div class="d-md-flex mb-3">
                        <h3 class="box-title mb-0">Tambah Produk</h3>
                    </div>
                    <div class="col-lg-8">
                        <form action="/produk/{{ $produk->id }}" method="post">
                            @csrf
                            @method('put')
                            <label for="nama_produk">Nama produk</label>
                            <input type="text" class="form-control @error('nama_produk') is-invalid @enderror" value="{{ $produk->nama_produk }}" name="nama_produk" id="nama_produk">
                            @error('nama_produk')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                            <label for="harga">Harga Produk</label>
                            <input type="number" min="0" class="form-control @error('harga') is-invalid @enderror" value="{{ $produk->harga }}" name="harga" id="harga">
                            @error('harga')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                            <button type="submit" class="btn btn-primary mt-3">Update</button>
                        </form> 
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Recent Comments -->
        <!-- ============================================================== -->
        
    </div>
@endsection