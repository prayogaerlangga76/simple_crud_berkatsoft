<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produk = Produk::all();
        $title = 'Kelola Produk';
        return view('backend.produk.index', compact('produk', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Tambah Produk';
        return view('backend.produk.create', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestAll = $request->validate([
            'nama_produk' => 'required',
            'harga' => 'required',
        ]);

        Produk::create($requestAll);
        Alert::success('Berhasil', 'Produk berhasil ditambahkan');
        return redirect('/produk')->with('success', 'Produk berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produk = Produk::find($id);
        return response()->json($produk);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produk = Produk::find($id);
        $title = 'Edit Produk';
        return view('backend.produk.edit', compact('produk', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestAll = $request->validate([
            'nama_produk' => 'required',
            'harga' => 'required',
        ]);
        Produk::where('id', $id)->update($requestAll);
        Alert::success('Berhasil', 'Produk berhasil diubah');
        return redirect('/produk')->with('success', 'Produk berhasil diubah');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Produk::destroy($id);
        Alert::success('Berhasil', 'Produk berhasil dihapus');
        return redirect('/produk')->with('success', 'Produk berhasil dihapus');;
    }
}
