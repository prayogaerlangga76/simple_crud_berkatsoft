<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customer = Customer::all();
        $title = 'Kelola Customer';
        return view('backend.customer.index', compact('customer', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Tambah Customer';
        return view('backend.customer.create', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestAll = $request->validate([
            'nama_customer' => 'required',
            'alamat' => 'required',
            'no_hp' => 'required'
        ]);
        Customer::create($requestAll);
        Alert::success('Berhasil', 'Customer berhasil ditambahkan');
        return redirect('/customer')->with('success', 'Customer berhasil ditambahkan');;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::find($id);
        $title = 'Edit Customer';
        return view('backend.customer.edit', compact('customer', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestAll = $request->validate([
            'nama_customer' => 'required',
            'alamat' => 'required',
            'no_hp' => 'required'
        ]);
        Customer::where('id', $id)->update($requestAll);
        Alert::success('Berhasil', 'Customer berhasil diubah');
        return redirect('/customer')->with('success', 'Customer berhasil diubah');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Customer::destroy($id);
        Alert::success('Berhasil', 'Customer berhasil dihapus');
        return redirect('/customer')->with('success', 'Customer berhasil dihapus');;
    }
}
