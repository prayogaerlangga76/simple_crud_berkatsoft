<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Produk;
use App\Models\Sale;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produk = Produk::all();
        $customers = Customer::all();
        $sales = Sale::all();
        $count = Sale::sum('total');
        $title = 'Kelola Sales';
        return view('backend.sales.index', compact('produk', 'customers', 'sales', 'title', 'count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $produk = Produk::all();
        $customers = Customer::all();
        $title = 'Tambah Sales';
        return view('backend.sales.create', compact('produk', 'customers', 'title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestAll = [
            'customer_id' => 'required',
            'produk_id1' => 'required',
            'jumlah1' => 'required',
            'total1' => 'required',
        ];

        if ($request->produk_id2) {
            $requestAll['produk_id2'] = 'required';
            $requestAll['jumlah2'] = 'required';
            $requestAll['total2'] = 'required';
        }

        $validatedData = $request->validate($requestAll);

        $sale = new Sale;
        $sale->customer_id = $validatedData['customer_id'];
        $sale->produk_id = $validatedData['produk_id1'];
        $sale->jumlah = $validatedData['jumlah1'];
        $sale->total = $validatedData['total1'];
        $sale->save();

        if ($request->produk_id2) {
            $sale = new Sale;
            $sale->customer_id = $validatedData['customer_id'];
            $sale->produk_id = $validatedData['produk_id2'];
            $sale->jumlah = $validatedData['jumlah2'];
            $sale->total = $validatedData['total2'];
            $sale->save();
        }
        Alert::success('Berhasil', 'Data berhasil ditambahkan');
        return redirect('/sales')->with('success', 'Sales berhasil ditambahkan');;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
