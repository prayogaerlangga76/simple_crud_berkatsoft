<?php

use App\Http\Controllers\AksesMenuController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\SalesController;
use App\Http\Controllers\SubmenuController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', [LoginController::class, 'index'])->name('login');
Route::post('/login', [LoginController::class, 'authenticate']);
Route::get('/registrasi', [LoginController::class, 'registrasi']);
Route::post('/registrasi', [LoginController::class, 'storeRegistrasi']);
Route::get('/logout', [LoginController::class, 'logout']);

Route::group(['middleware' => ['auth']], function () {
    Route::get('/', [DashboardController::class, 'index']);
    Route::resource('/menu', MenuController::class);
    Route::resource('/submenu', SubmenuController::class);
    Route::resource('/aksesmenu', AksesMenuController::class);
    Route::resource('/produk', ProdukController::class);
    Route::resource('/customer', CustomerController::class)->except('show');
    Route::resource('/sales', SalesController::class)->except('show');
});
